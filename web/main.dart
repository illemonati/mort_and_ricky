import 'dart:async';
import 'dart:html' as html;
import 'package:stagexl/stagexl.dart';
import 'dart:math';
import 'package:js/js_util.dart';
import 'package:js/js.dart' as js;
import 'dart:collection';


Future<Null> main() async {

  StageOptions options = StageOptions()
    ..backgroundColor = Color.HotPink
    ..inputEventMode = InputEventMode.MouseAndTouch
    ..renderEngine = RenderEngine.Canvas2D;

  int CANVAS_WIDTH = html.window.screen.available.width;
  int CANVAS_HEIGHT = html.window.screen.available.height - html.window.screen.available.height / 4;

  var canvas = html.querySelector('#stage');
  var stage = Stage(canvas, width: CANVAS_WIDTH, height: CANVAS_HEIGHT, options: options);
  bool debug = false;

  void debugPrint(Object thing) async {
    if (debug) {
      print(thing);
    }
  }

  void setDebug(bool bool) {
    debug = bool;
  }

  var renderLoop = RenderLoop();
  renderLoop.addStage(stage);


  BitmapDataLoadOptions jpgOptions = BitmapDataLoadOptions()..jpg = true;
  var resourceManager = ResourceManager();
  resourceManager.addBitmapData('morty', 'images/morty.jpg', jpgOptions);
  resourceManager.addBitmapData('pickle_rick', "images/Pickle_rick_transparent.png");
  resourceManager.addSound('damaged_cola', 'sound/damaged_cola.mp3');
  await resourceManager.load();

  BitmapData rickData = resourceManager.getBitmapData("pickle_rick");
  BitmapData mortyData = resourceManager.getBitmapData('morty');

  void tryUnlockMobileAudio() {
    stage.onTouchBegin.first.then((evt) {
      SoundMixer.unlockMobileAudio();
    });
  }

  void playSong() async {
    Sound damagedCola = resourceManager.getSound('damaged_cola');
    SoundChannel stream = damagedCola.play(true);
    tryUnlockMobileAudio();
  }

 



  Future<Sprite> addRick() async {
    Sprite rick = Sprite();
    rick.addChild(Bitmap(rickData));
    rick.pivotX = rickData.width / 2;
    rick.pivotY = rickData.height / 2;
    rick.scaleX = 0.25;
    rick.scaleY = 0.25;
    rick.x = CANVAS_WIDTH * Random().nextDouble();
    rick.y = 0;
    debugPrint('rick: (${rick.x}:${rick.y})');
    stage.addChild(rick);
    var rickTween= stage.juggler.addTween(rick, 3, Transition.easeOutBounce);
    rickTween.animate.y.to(CANVAS_HEIGHT * Random().nextDouble());
    Tween rotation;
    rick.onMouseClick.listen((MouseEvent e) {
      // Don't run more rotations at the same time.
      if (rotation != null) return;
      rotation = stage.juggler.addTween(rick, 0.5, Transition.easeInOutCubic);
      rotation.animate.rotation.by(2 * pi);
      rotation.onComplete = () => rotation = null;
    });
    await new Future.delayed(Duration(milliseconds: Random().nextInt(1200)));
    rotation = stage.juggler.addTween(rick, 0.5, Transition.easeInOutCubic);
    rotation.animate.rotation.by(2 * pi);
    rotation.onComplete = () => rotation = null;
    rick.mouseCursor = MouseCursor.CROSSHAIR;

    return rick;

//    await new Future.delayed(Duration(seconds: 30));
//    rick.removeFromParent();
  }

  Future<Sprite> addMorty() async {
    Sprite morty = Sprite();

    morty.addChild(Bitmap(mortyData));


    morty.pivotX = mortyData.width / 2;
    morty.pivotY = mortyData.height / 2;
    morty.scaleX = 0.25;
    morty.scaleY = 0.25;


    morty.x = CANVAS_WIDTH * Random().nextDouble();
    morty.y = 0;
    debugPrint('morty: (${morty.x}:${morty.y})');

    stage.addChild(morty);

    var mortyTween = stage.juggler.addTween(morty, 3, Transition.easeOutBounce);
    mortyTween.animate.y.to(CANVAS_HEIGHT * Random().nextDouble());
    Tween rotation;
    morty.onMouseClick.listen((MouseEvent e) {
      // Don't run more rotations at the same time.
      if (rotation != null) return;
      rotation = stage.juggler.addTween(morty, 0.5, Transition.easeInOutCubic);
      rotation.animate.rotation.by(-2 * pi);
      rotation.onComplete = () => rotation = null;
    });
    await new Future.delayed(Duration(milliseconds: Random().nextInt(1200)));
    rotation = stage.juggler.addTween(morty, 0.5, Transition.easeInOutCubic);
    rotation.animate.rotation.by(-2 * pi);
    rotation.onComplete = () => rotation = null;
    return morty;
//    await new Future.delayed(Duration(seconds: 30));
//    morty.removeFromParent();
  }

  void main_loop() async {
    playSong();
//    List<Sprite> spriteList = [];
    Queue<Sprite> spriteList = new Queue();
    bool has60 = false;

    void clearBoard() async{
      while (spriteList.isNotEmpty) {
        spriteList.removeFirst().removeFromParent();
      }
    }

    print('setdebug() to true to see debug info!');
    print('use addMorty() to add morty');
    print('use addRick() to add rick');
    print('use clearBoard() to clear board');
    setProperty(html.window, 'addRick', js.allowInterop(addRick));
    setProperty(html.window, 'addMorty', js.allowInterop(addMorty));
    setProperty(html.window, 'setDebug', js.allowInterop(setDebug));
    setProperty(html.window, 'clearBoard', js.allowInterop(clearBoard));
//    js.context['addRick'] = js.allowInterop(addRick);
//    js.context['addMorty'] = js.allowInterop(addMorty);
//    js.context['setDebug'] = js.allowInterop(setDebug);
//    js.context['clearBoard'] = js.allowInterop(clearBoard);

    while (true) {
      Sprite rick = await addRick();
      Sprite morty = await addMorty();
      if (has60) {
//        spriteList.removeRange(0, 2);
//        spriteList[0].removeFromParent();
//        spriteList[1].removeFromParent();
        spriteList.removeFirst().removeFromParent();
        spriteList.removeFirst().removeFromParent();
      } else {
        if (spriteList.length >= 60) {
          has60 = true;
//          spriteList[0].removeFromParent();
//          spriteList[1].removeFromParent();
//          spriteList.removeRange(0, 2);
          spriteList.removeFirst().removeFromParent();
          spriteList.removeFirst().removeFromParent();
        }
      }
      spriteList.add(rick);
      spriteList.add(morty);
      debugPrint(spriteList.length);
      await new Future.delayed(Duration(milliseconds: 500));
    }
  }



  await main_loop();



}

